FROM php:7.4-cli-alpine

RUN apk update \
    && apk upgrade \
    && apk add --no-cache \
    openssh-client \
    rsync

CMD ["php", "-a"]
